#coding: utf-8
import pyparsing as pp
import argparse

def hex_to_bin(opcode):
    return bin(int(opcode, 16))[2:].zfill(len(opcode)*4)

def bin_to_hex(opcode):
    return hex(int(opcode, 2))[2:].upper()

def parse_opcodes(filename):
    multibit_field = pp.Suppress("<") + pp.OneOrMore(pp.Word(pp.alphas+"_"))("name") + pp.OneOrMore(pp.Word(pp.nums))("width") + pp.Suppress(">")
    multifields = pp.OneOrMore(pp.Group(multibit_field("multibit_fields*")) ^
                               pp.Word(pp.alphas, exact=1) ^
                               pp.Literal("0") ^
                               pp.Literal("1"))
    opcodes_database = {}
    with open(filename, "r") as f:
        mnemomic = ""
        usage = ""
        for line in f:
            entry = {}
            if line.startswith("A6.7."):
                instruction = line.split(".")[-1].strip("\n")
                mnemomic = instruction.split(" (")[0] # Remove parenthesis for immediate or register
            elif line.startswith("Encoding"):
                encoding = line.strip("Encoding ").strip("\n")
            elif mnemomic and line.startswith(mnemomic):
                usage+=line.strip("\n")
                fields = []
            elif line.startswith("#"):
                opcode = line.split(" -> ")[1].replace("(", "").replace(")", "").strip("\n")
                try:
                    parsed_opcode = multifields.parseString(opcode, parseAll=True)
                except ParseException as e:
                    print e
                    print line
                    break
                masked_opcode = ""
                for item in parsed_opcode:
                    if item in ["0", "1"]:
                        masked_opcode+=item
                    elif isinstance(item, pp.ParseResults):
                        masked_opcode+=int(item[1])*"X"
                        fields.append(item)
                    else:
                        masked_opcode+="X"
                entry["instruction"] = instruction
                entry["encoding"] = encoding
                encoding = ""
                entry["usage"] = usage
                entry["masked_opcode"] = masked_opcode
                entry["fields"] = fields
                usage = ""
                opcodes_database[masked_opcode] = entry
    return opcodes_database

def HD_with_ignores(opcode_1, opcode_2):
    HD = 0
    if len(opcode_1) != len(opcode_2):
        return min(len(opcode_1), len(opcode_2))
    else:
        for bit_opcode_1, bit_opcode_2 in zip(opcode_1, opcode_2):
            if (bit_opcode_1, bit_opcode_2) in [("1", "0"), ("0", "1")]:
                HD+=1
        return HD

def get_altered_opcodes(opcode, alteration):
    altered_opcodes = []
    if alteration == "set":
        for index, bit in enumerate(opcode):
            if bit == "0":
                if index==0:
                    altered_opcodes.append((len(opcode)-index-1, "1"+opcode[1:]))
                elif index==len(opcode):
                    altered_opcodes.append((len(opcode)-index-1, opcode[:-1]+"1"))
                else:
                    altered_opcodes.append((len(opcode)-index-1, opcode[:index]+"1"+opcode[index+1:]))
    elif alteration == "reset":
        for index, bit in enumerate(opcode):
            if bit == "1":
                if index==0:
                    altered_opcodes.append((len(opcode)-index-1, "0"+opcode[1:]))
                elif index==len(opcode):
                    altered_opcodes.append((len(opcode)-index-1, opcode[:-1]+"0"))
                else:
                    altered_opcodes.append((len(opcode)-index-1, opcode[:index-1]+"0"+opcode[index+1:]))
    return altered_opcodes

def find_matches(opcode, opcodes_database):
    """
    Find the matches of a given opcode in the database
    """
    matches = []
    for masked_opcode in opcodes_database:
        if HD_with_ignores(masked_opcode, opcode) == 0:
            matches.append(opcodes_database[masked_opcode])
    return matches

def find_altered_matches(opcode, opcodes_database, fault):
    altered_opcodes = get_altered_opcodes(opcode, fault)
    altered_matches = []
    for altered_position, altered_opcode in altered_opcodes:
        altered_match = find_matches(altered_opcode, opcodes_database)
        if altered_match:
            altered_matches.append((altered_position, altered_match[0]))
    return altered_matches

def display_exact_and_altered_matches(opcode_to_analyse, opcodes_database, fault):
    print "Opcode:"
    print "               {}".format("   ".join([bit for bit in bin_to_hex(opcode_to_analyse)]))
    print "               {}".format(opcode_to_analyse)
    print "Exact matches"
    for exact_match in find_matches(opcode_to_analyse, opcodes_database):
        print ("               {}: {} {}".format(exact_match["masked_opcode"], exact_match["instruction"], exact_match["usage"]))
    print "Altered matches"
    for altered_position, altered_match in find_altered_matches(opcode_to_analyse, opcodes_database, fault):
        print "              "+(len(opcode_to_analyse)-altered_position)*" "+"⇓"+str(altered_position)
        print "               {}: {} {}".format(altered_match["masked_opcode"], altered_match["instruction"], altered_match["usage"])
        if altered_match["fields"]:
            for field in altered_match["fields"]:
                print "               "+len(opcode_to_analyse)*" "+": {}-bit {}".format(field[1], field[0])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Opcodes explorer')
    parser.add_argument("-b", "--binary_opcode_to_analyse", type=str, default=False)
    parser.add_argument("-x", "--hex_opcode_to_analyse", type=str, default=False)
    parser.add_argument("-f", "--fault", type=str, choices=["set", "reset"])
    args = parser.parse_args()

    if args.binary_opcode_to_analyse:
        opcode_to_analyse = args.binary_opcode_to_analyse
    elif args.hex_opcode_to_analyse:
        opcode_to_analyse = hex_to_bin(args.hex_opcode_to_analyse)
    else:
        raise ValueError ("You must provide an opcode to analyse")
                                    
    opcodes_database = parse_opcodes("arm_opcodes.txt")
    
    display_exact_and_altered_matches(opcode_to_analyse, opcodes_database, args.fault)
